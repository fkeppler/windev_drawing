﻿//assignment_4 by forrest keppler
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Shapes;
using System.Windows;
using System.Windows.Controls;

namespace drawing_assign
{
        /// <summary>
        /// Coord holds the info for a coordinate shape, adds coords to a canvas, and generates new coords
        /// </summary>
        class Coord
        {
            public Point start_point;
            public Point end_point;
            public Shape inside;

            public Coord(Point start, Point end, Shape the_shape)
            {
                start_point = start;
                end_point = end;
                inside = the_shape;
                inside.Width = Math.Max(end_point.X, start_point.X) - Math.Min(start_point.X, end_point.X);
                inside.Height = Math.Max(end_point.Y, start_point.Y) - Math.Min(start_point.Y, end_point.Y);
            }

            public Coord(int x1, int y1, int x2, int y2, Shape the_shape)
            {
                start_point = new Point
                {
                    X = x1,
                    Y = y1
                };
                end_point = new Point
                {
                    X = x2,
                    Y = y2
                };
                inside = the_shape;
                inside.Width = Math.Max(end_point.X, start_point.X) - Math.Min(start_point.X, end_point.X);
                inside.Height = Math.Max(end_point.Y, start_point.Y) - Math.Min(start_point.Y, end_point.Y);

            }
            /// <summary>
        /// adds a coord object to the canvas in DRAING!!!
        /// </summary>
            public void addToCanvas(Canvas canvas)
            {
                var x1 = Math.Min(start_point.X, end_point.X);
                var y1 = Math.Min(start_point.Y, end_point.Y);

                canvas.Children.Add(this.inside);
                Canvas.SetLeft(inside, x1);
                Canvas.SetTop(inside, y1);
            }
        }
    }
