﻿//assignment_4 by forrest keppler
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace drawing_assign
{
    /// <summary>
    /// Interaction logic for Line_dialogue.xaml
    /// </summary>
    public partial class Line_dialogue : UserControl
    {
        Window parent;
        public Line_dialogue()
        {
            InitializeComponent();
            parent = Window.GetWindow(this);
        }
        /// <summary>
        /// handles the user clicking ok. passes information back to DRAWING!!
        /// </summary>
        private void ok_Click(object sender, RoutedEventArgs e)
        {
            parent = Window.GetWindow(this);
            if (is_dashed.IsChecked == true)
            {
                Globals.line_type = 1;
            }
            else if (is_dotted.IsChecked == true)
            {
                Globals.line_type = 2;
            }
            else
            {
                Globals.line_type = 0;
            }

            Globals.line_width = (int)width_slider.Value + 2;
            parent.Close();
        }
        /// <summary>
        /// handles a "cancel" click. just closes the dialogue.
        /// </summary>
        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            parent = Window.GetWindow(this);
            parent.Close();
        }
    }
}
