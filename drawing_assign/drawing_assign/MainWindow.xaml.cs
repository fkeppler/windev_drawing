﻿//assignment_4 by forrest keppler
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace drawing_assign
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ShapeMaker tracker;
        Point start_point = new Point();
        Shpfile file = null;

        public MainWindow()
        {
            InitializeComponent();
            tracker = new ShapeMaker(ink_pad);
        }
        // handles left mouse button down, draws a shape via call to new shape.
        private void ink_pad_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            int[] new_shape = new int[14];
            if (start_point.X == 0 & start_point.Y == 0)
            {
                start_point = e.GetPosition(ink_pad);
            }
            else
            {
                new_shape[0] = Globals.shape_index;
                new_shape[1] = Globals.shouldFill;
                new_shape[2] = (int)start_point.X;
                new_shape[3] = (int)start_point.Y;
                new_shape[4] = (int)e.GetPosition(ink_pad).X;
                new_shape[5] = (int)e.GetPosition(ink_pad).Y;
                new_shape[6] = (int)Globals.fillColor.Color.R;
                new_shape[7] = (int)Globals.fillColor.Color.G;
                new_shape[8] = (int)Globals.fillColor.Color.B;
                new_shape[9] = (int)Globals.outline.Color.R;
                new_shape[10] = (int)Globals.outline.Color.G;
                new_shape[11] = (int)Globals.outline.Color.B;
                new_shape[12] = Globals.line_type;
                new_shape[13] = Globals.line_width;
                tracker.add_shape(new_shape);
                start_point.X = 0;
                start_point.Y = 0;
            }
        }



        #region bad stubs
        //unneeded method stubs
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        #endregion

        #region File menu
        private void Open_Click(object sender, RoutedEventArgs e)
        {
            //following code taken from http://msdn.microsoft.com/en-us/library/aa969773(v=vs.110).aspx
            // Configure open file dialog box
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = "Document"; // Default file name
            dlg.DefaultExt = ".txt"; // Default file extension
            dlg.Filter = "Text documents (.txt)|*.txt"; // Filter files by extension 

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results 
            if (result == true)
            {
                // Open document 
                string filename = dlg.FileName;
                //end of copied code
                file = new Shpfile(filename);
                tracker.clear();
                file.read_shapes(tracker);
            }
        }
        //saves the file via file.write shapes
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            if (file == null)
            {
                Save_as_Click(sender, e);
            }
            else
            {
                file.write_shapes(tracker);
            }
        }
        //handles the Save as button, saves using custom name defined by user.
        private void Save_as_Click(object sender, RoutedEventArgs e)
        {
            //code taken from http://msdn.microsoft.com/en-us/library/aa969773(v=vs.110).aspx
            // Configure save file dialog box
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "Document"; // Default file name
            dlg.DefaultExt = ".text"; // Default file extension
            dlg.Filter = "Text documents (.txt)|*.txt"; // Filter files by extension 

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results 
            if (result == true)
            {
                // Save document 
                string filename = dlg.FileName;
                //end copied code
                file = new Shpfile(filename);
                file.write_shapes(tracker);
            }
        }
        //handles the user closing the application. (TODO: make prompt the user for save to prevent losing work on accidental close.)
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        #endregion

        #region Edit menu
        //finds the last shape drawn from allShapes and removes it from the canvas, then adds it to the last_undone queue
        private void Undo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                tracker.allShapes.Remove(Globals.last_shape);
                tracker.draw_all();
                Globals.last_undone = Globals.last_shape;
                Globals.last_shape = null;
            }
            catch
            {
                MessageBox.Show("cannot undo");
            }


        }
        //gets the last shape from the last_undone queue and adds it back to the canvas.
        private void Redo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                tracker.allShapes.Add(Globals.last_undone);
                tracker.draw_all();
                Globals.last_undone = null;
            }
            catch
            {
                MessageBox.Show("nothing to redo");
            }
        }
        #endregion

        #region image menu
        //clears all the shapes from the canvas.
        private void clear_screen_Click(object sender, RoutedEventArgs e)
        {
            tracker.clear();
        }

        private void open_background_Click(object sender, RoutedEventArgs e)
        {
            //following code taken from http://msdn.microsoft.com/en-us/library/aa969773(v=vs.110).aspx
            // Configure open file dialog box
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = "Document"; // Default file name
            dlg.Filter = "PNG|*.png|DOT|*.dot|Windows Bitmap Format|*.bmp|GIF|*.gif|JPEG|*.jpg"; // Filter files by extension 

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {
                // Open document 
                string filename = dlg.FileName;
                //end of copied code
                file = new Shpfile(filename);
                ink_pad.Background = new ImageBrush(new BitmapImage(new Uri(filename)));
            }
        }
        //saves the image using SaveCanvas()
        private void save_image_Click(object sender, RoutedEventArgs e)
        {
            if (Globals.image_out != null)
            {
                SaveCanvas(this, this.ink_pad, 96, Globals.image_out);
            }
            else
            {
                MenuItem_Click(sender, e);
            }

        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
                        //code taken from http://msdn.microsoft.com/en-us/library/aa969773(v=vs.110).aspx
            // Configure save file dialog box
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "Document"; // Default file name
            dlg.Filter = "PNG|*.png|DOT|*.dot|Windows Bitmap Format|*.bmp|GIF|*.gif|JPEG|*.jpg"; // Filter files by extension 
            dlg.DefaultExt = ".bmp";

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results 
            if (result == true)
            {
                // Save document 
                Globals.image_out = dlg.FileName;
                //end copied code
                SaveCanvas(this, this.ink_pad, 96, Globals.image_out);
            }
        }
       /// <summary>
       /// this is used for saving the canvas
       /// </summary>
       /// <param name="window"></param>
       /// <param name="canvas"></param>
       /// <param name="dpi"></param>
       /// <param name="filename"></param>
        public static void SaveCanvas(Window window, Canvas canvas, int dpi, string filename) 
        { 
            Size size = new Size(window.Width , window.Height ); 
            canvas.Measure(size); 
            //canvas.Arrange(new Rect(size));

            var rtb = new RenderTargetBitmap( 
                (int)window.Width, //width 
                (int)window.Height, //height 
                dpi, //dpi x 
                dpi, //dpi y 
                PixelFormats.Pbgra32 // pixelformat 
                ); 
            rtb.Render(canvas);

            SaveRTBAsPNG(rtb, filename); 
        }
        /// <summary>
        /// used for saving canvas as png
        /// </summary>
        /// <param name="bmp"></param>
        /// <param name="filename"></param>
         private static void SaveRTBAsPNG(RenderTargetBitmap bmp, string filename) 
        { 
            var enc = new System.Windows.Media.Imaging.PngBitmapEncoder(); 
            enc.Frames.Add(System.Windows.Media.Imaging.BitmapFrame.Create(bmp));

            using (var stm = System.IO.File.Create(filename)) 
            { 
                enc.Save(stm); 
            } 
        } 

# endregion
        //handles clicking on the "line" menu item
         private void Line_Click(object sender, RoutedEventArgs e)
         {
             Window lineInfo = new Window
             {
                 Title = "LineParameters",
                 Content = new Line_dialogue(),
                 SizeToContent = System.Windows.SizeToContent.WidthAndHeight
             };
             lineInfo.ShowDialog();
         }
        //opens a dialog for finding the users desired fill color
         #region Color
         private void Fill_Click(object sender, RoutedEventArgs e)
         {

             //code taken from http://searchwindevelopment.techtarget.com/tip/Working-with-ColorDialog-in-WPF
             var dialog = new System.Windows.Forms.ColorDialog();
             if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
             {
                 Globals.fillColor = new SolidColorBrush(Color.FromArgb(dialog.Color.A, dialog.Color.R, dialog.Color.G, dialog.Color.B));
                 background_box.Background = Globals.fillColor;
             }

         }
        //opens a dialog to ask the user about how they want the shapes line
         private void outline_Click(object sender, RoutedEventArgs e)
         {
             //code taken from http://searchwindevelopment.techtarget.com/tip/Working-with-ColorDialog-in-WPF
             var dialog = new System.Windows.Forms.ColorDialog();
             if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
             {
                 Globals.outline = new SolidColorBrush(Color.FromArgb(dialog.Color.A, dialog.Color.R, dialog.Color.G, dialog.Color.B));
             }
         }
        //sets the background to the users desired color.
         private void background_Click(object sender, RoutedEventArgs e)
         {
                 //code taken from http://searchwindevelopment.techtarget.com/tip/Working-with-ColorDialog-in-WPF
             var dialog = new System.Windows.Forms.ColorDialog();
             if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
             {
                 ink_pad.Background = new SolidColorBrush(Color.FromArgb(dialog.Color.A, dialog.Color.R, dialog.Color.G, dialog.Color.B));
             }
         }
        //changes whether or not drawn items have a fill color.
         private void MenuItem_Click_2(object sender, RoutedEventArgs e)
         {
             if (Globals.shouldFill == 0)
             {
                 Globals.shouldFill = 1;
             }
             else
             {
                 Globals.shouldFill = 0;
             }
         }
#endregion
        //shows the "about the author" box
         private void MenuItem_Click_1(object sender, RoutedEventArgs e)
         {
             AboutBox1 dialog = new AboutBox1();
             dialog.ShowDialog();
         }
        //changes the shape that the program is drawing.
         private void comboBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
         {
             Globals.shape_index = comboBox1.SelectedIndex;
         }
            //following methods handle the various clicks on the UI
         #region toolstrip items
         private void Button_Click(object sender, RoutedEventArgs e)
         {
             Open_Click(sender, e);
         }

         private void Button_Click_1(object sender, RoutedEventArgs e)
         {
             Save_Click(sender, e);
         }

         private void Button_Click_2(object sender, RoutedEventArgs e)
         {
             Undo_Click(sender, e);
         }

         private void Button_Click_3(object sender, RoutedEventArgs e)
         {
             Redo_Click(sender, e);
         }

         private void Button_Click_4(object sender, RoutedEventArgs e)
         {
             MenuItem_Click_2(sender, e);
         }

         private void Button_Click_5(object sender, RoutedEventArgs e)
         {
             Line_Click(sender, e);
         }

         private void Button_Click_6(object sender, RoutedEventArgs e)
         {
             Open_Click(sender, e);
         }

         private void Button_Click_7(object sender, RoutedEventArgs e)
         {
             save_image_Click(sender, e);
         }

         private void Button_Click_8(object sender, RoutedEventArgs e)
         {
             background_Click(sender, e);
         }

         private void Button_Click_9(object sender, RoutedEventArgs e)
         {
             Fill_Click(sender, e);
         }
         
         private void Button_Click_10(object sender, RoutedEventArgs e)
         {
             outline_Click(sender, e);
         }

         private void Button_Click_11(object sender, RoutedEventArgs e)
         {
             MenuItem_Click_1(sender, e);
         }
#endregion
        //updates the box that shows mouse position.
         private void ink_pad_MouseMove(object sender, MouseEventArgs e)
         {
             mouse_box.Text = "mouse position = (" + Mouse.GetPosition(ink_pad).ToString() + ")";
         }



    }
}
