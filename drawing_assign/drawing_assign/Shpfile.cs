﻿//assignment_4 by forrest keppler
using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Shapes;
using System.Windows;
using System.Windows.Controls;
using System.IO;

namespace drawing_assign
{
    class Shpfile
    {
        string fileName;

        public Shpfile(string inFileName)
        {
            fileName = inFileName;
        }
        /// <summary>
        ///  loads shapes from a file onto the canvas.
        /// </summary>
        public void read_shapes(ShapeMaker shapes)
        {
            StreamReader reader = new StreamReader(fileName);
            string current = reader.ReadLine();
            ///first grab the background color
            string[] pieces = current.Split(' ');
            shapes.canvas.Background = new SolidColorBrush
            {
                Color = Color.FromRgb(
                    Convert.ToByte(pieces[0]),
                    Convert.ToByte(pieces[1]),
                    Convert.ToByte(pieces[2]))
            };

            //next grab the 'is there a background image'
            current = reader.ReadLine();
            if (current != "0")
            {
                pieces = current.Split(' ');
                shapes.canvas.Background = new ImageBrush
                {
                    ImageSource = new BitmapImage(new Uri("@" + pieces[1]))
                };
            }
            //grab and throw away num images
            reader.ReadLine();
            //enter loop to add images to read_shapes
            while (reader.Peek() != -1)
            {
                current = reader.ReadLine();
                pieces = current.Split(' ');
                shapes.add_shape(pieces);
            }
            // close the reader
            reader.Close();
        }
        /// <summary>
        /// writes shapes from the canvas to a file. "pickling"
        /// </summary>
        public void write_shapes(ShapeMaker shapes)
        {
            StreamWriter writer = new StreamWriter(fileName);
            SolidColorBrush temp;
            string current;
            //first line is background color.
            try
            {
                temp = (SolidColorBrush)shapes.canvas.Background;
                current = "";
                current += temp.Color.R.ToString() + " ";
                current += temp.Color.G.ToString() + " ";
                current += temp.Color.B.ToString();
                writer.WriteLine(current);
            }
            catch
            {
                writer.WriteLine("0, 0, 0");
            }


            //second line is the background image
            try
            {
                current = "1 ";
                ImageSourceConverter temp2 = new ImageSourceConverter();
                current += temp2.ConvertTo((ImageBrush)shapes.canvas.Background, current.GetType());
                writer.WriteLine(current);
            } catch {
                writer.WriteLine("0");
            }

            //third line is an integer with the number of shapes.
            current = Convert.ToString(shapes.allShapes.Count);
            writer.WriteLine(current);
            //all future lines involve inscribing shapes
            foreach (Coord shape in shapes.allShapes) {
                int [] pieces = new int [14];
                //first put the index of the shape
                pieces[0] = ShapeMaker.shapeToIndex(shape.inside);
                //next put whether the shape is filled or not.
                //also does the fill colors
                try {
                    temp = (SolidColorBrush)shape.inside.Fill;
                    pieces [1] = 1;
                    pieces [6] = temp.Color.R;
                    pieces [7] = temp.Color.G;
                    pieces [8] = temp.Color.B;

                } catch {
                    pieces[1] = 0;}
                //next the start point
                pieces[2] = (int)shape.start_point.X;
                pieces[3] = (int)shape.start_point.Y;
                //then the end point
                pieces[4] = (int)shape.end_point.X;
                pieces[5] = (int)shape.end_point.Y;
                //then the border color
                temp = (SolidColorBrush)shape.inside.Stroke;
                pieces [9] = temp.Color.R;
                pieces [10] = temp.Color.G;
                pieces [11] = temp.Color.B;

                //then the border type
                if (shape.inside.StrokeDashArray == Globals.dashed_line) {
                    pieces [12] = 1;
                } else if (shape.inside.StrokeDashArray == Globals.dotted_line) {
                    pieces [12] = 2;
                } else {
                    pieces [12] = 0;
                }
                //then the border width
                pieces [13] = (int)shape.inside.StrokeThickness;

                //finally, convert and write out the line
                current = "";
                foreach (int x in pieces) {
                    current += Convert.ToString(x) + " ";
                }
                writer.WriteLine(current);
 
        }
            writer.Close();
        }
    }
}
