﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace drawing_assign
{
    static class Globals
    {
        /// <summary>
        /// these represent the types necessary to get a dotted or dashed line.
        /// </summary>
        public static DoubleCollection dotted_line = new DoubleCollection(new double [] {1, 1});
        public static DoubleCollection dashed_line = new DoubleCollection(new double [] {3, 2});

        /// <summary>
        /// for the undo function
        /// </summary>
        public static Coord last_shape = null;
        public static Coord last_undone = null;

        /// <summary>
        /// parameters for shapes
        /// </summary>
        public static SolidColorBrush outline = Brushes.Black;
        public static int line_width = 2;
        public static int line_type = 0;
        public static SolidColorBrush fillColor = Brushes.White;
        public static int shouldFill = 0;//0 for false, 1 for true
        public static int shape_index = 0;

        /// <summary>
        /// file names
        /// </summary>
        public static string image_out = null;
    }
}
