﻿//assignment_4 by forrest keppler
using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Linq;
using System.Text;
using System.Windows.Shapes;
using System.Windows;
using System.Windows.Controls;

namespace drawing_assign
{
    /// <summary>
    /// a ShapeMaker object is used to keep track of the coordinate shapes displayed on the canvas, removes them, writes and reads them from a file.
    /// </summary>
    class ShapeMaker
    {
        public Canvas canvas;
        public List<Coord> allShapes = new List<Coord>();
        SolidColorBrush color;

        public ShapeMaker(Canvas in_canvas)
        {
            canvas = in_canvas;
            color = Brushes.Aquamarine;
        }

        public void clear()
        {
            allShapes = new List<Coord>();
            canvas.Children.Clear();
            canvas.Background = Brushes.White;
        }

        public void draw_all()
        {
            canvas.Children.Clear();
            foreach (Coord x in allShapes) {
                if (x != null)
                {
                    x.addToCanvas(canvas);
                }
            }
        }
        /// <summary>
        /// grab a shape based on the number.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public static Shape shapeFromIndex(int index, int width, int height)
        {
            Polygon final = new Polygon();
            PointCollection points = new PointCollection();
            switch (index)
            {
                case 0: return new Rectangle
                {
                    Height = height,
                    Width = width
                };
                case 1: return new Ellipse
                {
                    Height = height,
                    Width = width
                };
                    //right triangle
                case 2:
                    points.Add(new Point(0, 0));
                    points.Add(new Point(0, height));
                    points.Add(new Point(width, height));
                    break;
                    //Isocilese triangle
               case 3:
                    points.Add(new Point(0, 0));
                    points.Add(new Point(width, 0));
                    points.Add(new Point(width/2, height));
                    break;
                    //diamond
                case 4:
                    points.Add(new Point(0, height / 2));
                    points.Add(new Point(width / 2, 0));
                    points.Add(new Point(width, height / 2));
                    points.Add(new Point(width / 2, height));
                    break;
                    //hexagon
                case 5:
                    points.Add(new Point(width / 3, 0));
                    points.Add(new Point(2 * width / 3, 0));
                    points.Add(new Point(width, height / 2));
                    points.Add(new Point(2 * width / 3, height));
                    points.Add(new Point(width / 3, height));
                    points.Add(new Point(0, height / 2));
                    break;
                    //pentagon
                case 6:
                    points.Add(new Point(width / 2, 0));
                    points.Add(new Point(0, height / 2));
                    points.Add(new Point(width / 3, height));
                    points.Add(new Point(2 * width / 3, height));
                    points.Add(new Point(width, height / 2));
                    break;
            }
            final.Points = points;
            return final;
        }
        /// <summary>
        /// tells what shapemaker index a given shape corresponds too.
        /// </summary>
        public static int shapeToIndex (Shape input) {
            Shape tester;
            try
            {
                tester = (Rectangle)input;
                return 0;
            }
            catch { }
            try
            {
                tester = (Ellipse)input;
                return 1;
            }
            catch { }
            Polygon test = (Polygon)input;
            switch (test.Points.Count)
            {
                case 3: return 2;
                case 4: return 4;
                case 5: return 5;
                case 6: return 6;
            }
            return 0;
        }
        /// <summary>
        /// adds a shape to the tracker, then to the canvas. obsolete function
        /// </summary>
        /// <param name="point1"></param>
        /// <param name="point2"></param>
        public void add_shape(Point point1, Point point2, int index)
        {
            int shape_height = Math.Min((int)point1.Y, (int)point2.Y) - Math.Max((int)point2.Y, (int)point1.Y);
            int shape_width = Math.Min((int)point1.X, (int)point2.X) - Math.Max((int)point2.X, (int)point1.X);
            Shape example = shapeFromIndex(index, shape_width, shape_height);

            example.Stroke = color;
            example.StrokeThickness = 2;

           Coord new_coord = new Coord(point1, point2, example);
           allShapes.Add(new_coord);
           draw_all();

            //necessary for undo
           Globals.last_shape = new_coord;
        }
        /// <summary>
        /// for taking in shapes from a .shp file.
        /// </summary>
        /// <param name="input"></param>
        public void add_shape(int[] input)
        {
            //initialize the correct new shape
            int shape_height = Math.Max(input[3], input[5]) - Math.Min(input[3], input[5]);
            int shape_width = Math.Max(input[2], input[4]) - Math.Min(input[2], input[4]);
            Shape added = shapeFromIndex(input[0], shape_width, shape_height);
            // create the shapes fill color
            if (input[1] == 1)
            {
                added.Fill = new SolidColorBrush
                {
                    Color = Color.FromRgb(
                        Convert.ToByte(input[6]),
                        Convert.ToByte(input[7]),
                        Convert.ToByte(input[8]))
                };
            }
            //change border color
            added.Stroke = new SolidColorBrush
            {
                Color = Color.FromRgb(
                    Convert.ToByte(input[9]),
                    Convert.ToByte(input[10]),
                    Convert.ToByte(input[11]))
            };
            //change border thickness
            added.StrokeThickness = input[13];
            //change border type
            switch (input[12])
            {
                case 1:
                    added.StrokeDashArray = Globals.dashed_line;
                    break;
                case 2:
                    added.StrokeDashArray = Globals.dotted_line;
                    break;
            }
            //create the coord and add to all shapes
            Coord new_draw = new Coord(input[2], input[3], input[4], input[5], added);
            allShapes.Add(new_draw);
            this.draw_all();

            //necessary for undo button
            Globals.last_shape = new_draw;

        }
        /// <summary>
        /// adds a Shape to the canvas.
        /// </summary>
        public void add_shape(string[] input)
        {
            int[] converted = new int[input.Length];
            for (int i = 0; i < input.Length-1; i++)
            {
                converted[i] = Convert.ToInt32(input[i]);
            }
            add_shape(converted);
        }
    }
}
